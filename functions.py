import re
import math
import operator
import string

#returns the n'th element in the fibbonaci sequqnce
def n_fibbo_element(n):
    element = 0
    a,b = 0,1
    for i in range(n-1):
        a,b =b,a+b
        element = a
    return element


#return a n sized list of fibbonaci elements
def list_n_fibbo_elements(size):
    elements = []
    a,b = 0,1
    elements.append(a)
    for i in range(size-1):
        a,b = b,a+b
        elements.append(a)

    return elements


#return the factorial of the given number
def factorial(number):
    result = 1
    len = number+1
    for i in range(1,len):
        result = result * i

    return result


#returns the net balance of an account after a series of operations which respect the following rules
# D - means deposit operation
# W - means withdrawal operation
# input type of this function is a list of operations
# output is the resulting amount
# example: net_amount(['D 500', 'W 200'])
# output should be: 300
def net_amount(history):
    net_balance = 0

    for h in history:
        values = h.split(" ")
        operation = values[0]
        amount = int(values[1])
        if operation=="D":
            net_balance+=amount
        elif operation=="W":
            net_balance-=amount
        else:
            pass

    return net_balance


#checks if a password is valid according to the following cryteria:
# must contain lower case letters
# must contain upper case letters
# must contain numbers
# must contain special characters
def check_password(password):
    if len(password)<6 or len(password)>18:
        return False

    if not re.search("[a-z]",password):
        return False
    elif not re.search("[0-9]",password):
        return False
    elif not re.search("[A-Z]",password):
        return False
    elif not re.search("[$#@]",password):
        return False
    elif re.search("\s",password):
        return False
    else:
        return True


# returns the distance between the starting position and the final position
# based on the matriceal movement provided
# input list of moves (UP number, DOWN number, RIGHT number, LEFT number)
# output - distance between start and finish
# example input: ['DOWN 5', 'RIGHT 3', 'UP 3', 'RIGHT 2']
# resulting output: 5
def matrix_movement(moves):
    pos = [0,0]

    for m in moves:
        movement = m.split(" ")
        direction = movement[0]
        steps = int(movement[1])
        if direction=="UP":
            pos[0]+=steps
        elif direction=="DOWN":
            pos[0]-=steps
        elif direction=="LEFT":
            pos[1]-=steps
        elif direction=="RIGHT":
            pos[1]+=steps
        else:
            pass

    return int(round(math.sqrt(pos[1]**2+pos[0]**2)))


# returns the most frequent word from a sentence
def word_freq(sentence):
    
    exclude = set(string.punctuation)
    sentence = ''.join(ch for ch in sentence if ch not in exclude)
    sentence = sentence.lower()
    freq = {}
    for word in sentence.split(" "):
        freq[word] = freq.get(word,0)+1

    sorted_freq = sorted(freq.items(), key=operator.itemgetter(1), reverse=True)
    return list(sorted_freq)[0]


#returns a number after trying to convert it from a string
def string_to_int(input):

    if(isinstance(input, str)):
        try:
            result = int(input)
            return result
        except:
            raise ValueError("Input cannot be converted to a number")
    else:
        raise ValueError("Input is already a number")

    
# return something -> find out what
def recursive(n):
    if n==0:
        return 0
    else:
        return recursive(n-1)+100


# searches for the position of an element within a list
# input - list of elements & element we need to find
def bin_search(li, element):
    bottom = 0
    top = len(li)-1
    index = -1
    while top>=bottom and index==-1:
        mid = int(math.floor((top+bottom)/2.0))
        if li[mid]==element:
            index = mid
        elif li[mid]>element:
            top = mid-1
        else:
            bottom = mid+1

    return index